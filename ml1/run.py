from sklearn.externals import joblib

import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.svm import SVC
from sklearn.cross_validation import ShuffleSplit
from sklearn.grid_search import GridSearchCV

from load_dataset import dataset, labels, repository

# Ensure that there are no NaNs
dataset = dataset.fillna(-85)

# Split the dataset into training (90 \%) and testing (10 \%)
X_train, X_test, y_train, y_test = train_test_split(dataset, labels,
test_size = 0.1 )

cv = ShuffleSplit(X_train.shape[0], n_iter=10, test_size=0.2,
random_state=0)


# Define the classifier to use
estimator = SVC(kernel='linear')

# Define parameter space.
gammas = np.logspace(-6, -1, 10)

# Use Test dataset and use cross validation to find bet hyper-parameters.
print "Starting loading classifier data"
from sklearn.externals import joblib
classifier = joblib.load( 'ml1.pkl') 


#~ classifier.fit(X_train, [repository.locations.keys().index(tuple(l)) for
#~ l in y_train])

print "Scoring the classification"
# Test final results with the testing dataset
score = classifier.score(X_test, [repository.locations.keys().index(tuple(l)) for
l in y_test])
print "Final score is: ", score

